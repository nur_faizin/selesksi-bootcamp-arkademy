<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Person;
use App\Region;


class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $person = Person::all();
        $region = Region::all();
        return view('person.index',compact('person','region'));
    }

    public function summary()
    {
        $summary =  Person::groupBy('region_id')->get();
        $summaryCount = $summary->count();
        dd($summary,$summaryCount);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'income' => 'required',
            'region' => 'required',
            'alamat' => 'required'
        ]);

        $person = new Person();
        $person->name = $request->get('name');
        $person->income = $request->get('income');
        $person->region_id = $request->get('region');
        $person->address = $request->get('alamat');
        $person->save();

        return redirect('/person')->with('message','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $person = Person::find($id);
        return view('person.show',compact('person'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $person = Person::find($id);
        $region = Region::all();
        return view('person.edit',compact('person','region'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'income' => 'required',
            'region' => 'required',
            'alamat' => 'required'
        ]);


        $person =  Person::find($id);
        $person->name = $request->get('name');
        $person->income = $request->get('income');
        $person->region_id = $request->get('region');
        $person->address = $request->get('alamat');
        $person->save();

        return redirect('/person')->with('message','success');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $person = Person::find($id);
        $person->delete();
        return redirect('/person')->with('message','deleted');
    }
}
