<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'regions';

    public function getPerson()
    {
        return $this->hasMany('App\Person','region_id','id');
    }
}
