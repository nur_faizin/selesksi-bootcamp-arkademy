<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = 'persons';

    public function getRegion()
    {
        return $this->belongsTo('App\Region','region_id','id');
    }
}
