@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Summary</div>

                <div class="card-body">
                   
                        <div class="card-body">
                               
                                <table class="table table-bordered">
                                    <thead>
                                      <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Nama Daerah</th>
                                        <th scope="col">Jumlah Penduduk</th>
                                        <th scope="col">Total Pendapatan</th>
                                        <th scop="col">Rata-Rata Pendapatan</th>
                                        <th scope="col">Status</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      @foreach ($summary as $index => $item)
                                          <tr>
                                          <td>{{$index+1}}</td>
                                          <td>{{$item->name}}</td>
                                          <td>{{count($item->getPerson)}}</td>
                                          <td>{{$item->getPerson->sum('income')}}</td>
                                          <td>{{$item->getPerson->avg('income')}}</td>
                                          <td>
                                            @if($item->getPerson->avg('income')>2200000)
                                            <span  style="color:green">hijau</span>
                                            @elseif($item->getPerson->avg('income')>1700000)
                                            <span style="color:yellow">Kuning</span>
                                            @else
                                                <span style="color:red">merah</span>
                                            @endif
                                          </td>
                                          </tr>
                                      @endforeach
                                    </tbody>
                                  </table>
            
            
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
