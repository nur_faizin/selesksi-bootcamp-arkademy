@extends('layouts.app')
@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Person</div>

                <div class="card-body">
                    <button data-toggle="modal" data-target="#addPerson"  class="btn btn-success" style="float:right">Tambah Person</button>
                    <div class="clearfix"></div>
                    <br>
                    <input class="form-control" id="myInput" type="text" placeholder="Search..">
                    <br>
                    <table id="myTable" class="table table-bordered">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Penduduk</th>
                            <th scope="col">Gaji</th>
                            <th scope="col">Daerah</th>
                            <th scop="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($person as $index => $item)
                            <tr>
                            <td>{{$index +1}} </td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->income}}</td>
                            <td>{{$item->getRegion->name}}</td>
                            <td align="center">
                                <a href="{{route('person.edit',$item->id)}}" class="btn-sm btn-success">Ubah</a>
                                <a href="{{route('person.show',$item->id)}}" class="btn-sm btn-primary">Lihat</a>
                                <form action="{{route('person.destroy',$item->id)}}" method="post">
                                  {{ method_field('delete')}}
                                  @csrf
                                <button href="{{route('person.destroy',$item->id)}}" class="btn-sm btn-danger">Hapus</button>
                                </form>  
                            </td>
                          @endforeach
                        </tbody>
                      </table>


                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade bs-example-modal-lg" id="addPerson" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <form id="demo-form1" method="post" action="{{route('person.store')}}" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                  
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-6 col-sm-6 col-xs-12" for="name">Nama <span class="required"></span>
                        </label>
                        <div class="col-md-6  col-xs-12">
                            <input type="text" name="name" required="required" class="form-control  col-xs-4">
                            <ul class="parsley-errors-list filled" style="display:none" id="parsley-id-5"><li class="parsley-required">This value is required.</li></ul>
                        </div>
                    </div>

                    <div class="form-group">
                      <label class="col-md-6 col-sm-6 col-xs-12" for="name">Gaji <span class="required"></span>
                      </label>
                      <div class="col-md-6  col-xs-12">
                          <input type="text" name="income" required="required" class="form-control  col-xs-4">
                          <ul class="parsley-errors-list filled" style="display:none" id="parsley-id-5"><li class="parsley-required">This value is required.</li></ul>
                      </div>
                  </div>

                  <div class="form-group">
                      <label class="col-md-6 col-sm-6 col-xs-12" for="name">Region <span class="required"></span>
                      </label>
                      <div class="col-md-6  col-xs-12">
                          <select  name="region" required="required" class="form-control  col-xs-4">
                          @foreach ($region as $item)
                          <option value="{{$item->id}}">{{$item->name}}</option>
                          @endforeach  
                          </select>
                          <ul class="parsley-errors-list filled" style="display:none" id="parsley-id-5"><li class="parsley-required">This value is required.</li></ul>
                      </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-6 col-sm-6 col-xs-12" for="name">alamat <span class="required"></span>
                    </label>
                    <div class="col-md-6  col-xs-12">
                        <textarea name="alamat" required="required" class="form-control  col-xs-4">
                        </textarea>
                        <ul class="parsley-errors-list filled" style="display:none" id="parsley-id-5"><li class="parsley-required">This value is required.</li></ul>
                    </div>
                </div>
                  
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>


    
@endsection
@section('extrajs')
<script>
        $(document).ready(function(){
            $("#myInput").on("keyup", function() {
              var value = $(this).val().toLowerCase();
              $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
              });
            });
          });
</script>    
@endsection