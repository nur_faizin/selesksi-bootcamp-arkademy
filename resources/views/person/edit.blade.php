@extends('layouts.app')
@section('content')

<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <br>
                <form action="{{route('person.update',$person->id)}}" method="POST">
                    <input type="hidden" name="_method" value="PUT">
                    @csrf
                        <div class="form-group">
                            <label class="col-md-6 col-sm-6 col-xs-12" for="name">Nama <span class="required"></span>
                            </label>
                            <div class="col-md-6  col-xs-12">
                            <input type="text" name="name" value="{{$person->name}}" required="required" class="form-control  col-xs-4">
                                <ul class="parsley-errors-list filled" style="display:none" id="parsley-id-5"><li class="parsley-required">This value is required.</li></ul>
                            </div>
                        </div>

                         <div class="form-group">
                            <label class="col-md-6 col-sm-6 col-xs-12" for="name">Gaji <span class="required"></span>
                            </label>
                            <div class="col-md-6  col-xs-12">
                                <input type="text" name="income"  value="{{$person->income}}" required="required" class="form-control  col-xs-4">
                                <ul class="parsley-errors-list filled" style="display:none" id="parsley-id-5"><li class="parsley-required">This value is required.</li></ul>
                            </div>
                        </div>


                        <div class="form-group">
                                <label class="col-md-6 col-sm-6 col-xs-12" for="name">Region <span class="required"></span>
                                </label>
                                <div class="col-md-6  col-xs-12">
                                    <select  name="region" required="required" class="form-control  col-xs-4">
                                    @foreach ($region as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach  
                                    </select>
                                    <ul class="parsley-errors-list filled" style="display:none" id="parsley-id-5"><li class="parsley-required">This value is required.</li></ul>
                                </div>
                        </div>

                        <div class="form-group">
                                <label class="col-md-6 col-sm-6 col-xs-12" for="name">alamat <span class="required"></span>
                                </label>
                                <div class="col-md-6  col-xs-12">
                                    <textarea name="alamat"  required="required" class="form-control  col-xs-4"> {{$person->address}}</textarea>
                                    <ul class="parsley-errors-list filled" style="display:none" id="parsley-id-5"><li class="parsley-required">This value is required.</li></ul>
                                </div>
                            </div>

                        <div class="form-group">    
                            <div class="col-md-6  col-xs-12">
                                 <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                           
                        </div>    
                    </form>
                   

                </div>
            </div>
        </div>
</div>
    
@endsection