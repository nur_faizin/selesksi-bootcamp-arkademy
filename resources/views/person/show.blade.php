@extends('layouts.app')
@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header">{{$person->name}}</div>

                <div class="card-body">
                   
                    <div class="title m-b-md">
                        Nama : {{$person->name}}          
                    </div>

                    <div class="title m-b-md">
                        Income : Rp.{{$person->income}}          
                    </div>

                    <div class="title m-b-md">
                         Address : {{$person->address}}          
                    </div>

                    <div class="title m-b-md">
                        region : {{$person->getRegion->name}}          
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>





    
@endsection
