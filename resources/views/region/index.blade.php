@extends('layouts.app')
@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Region</div>

                <div class="card-body">
                    <button data-toggle="modal" data-target="#addRegion"  class="btn btn-success" style="float:right">Tambah Region</button>
                    <div class="clearfix"></div>
                    <br>
                    <input class="form-control" id="myInput" type="text" placeholder="Search..">
                    <br>

                    <table id="myTable" class="table table-bordered">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Region</th>
                            <th scope="col">Created At</th>
                            <th scope="col">Updated At</th>
                           
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($region as $index => $item)
                            <tr>
                            <td>{{$index +1}} </td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->created_at}}</td>
                            <td>{{$item->updated_at}}</td>
                         
                          @endforeach
                        </tbody>
                      </table>


                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade bs-example-modal-lg" id="addRegion" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <form id="demo-form1" method="post" action="{{route('region.store')}}" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                  
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-6 col-sm-6 col-xs-12" for="name">Nama Region<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="region" required="required" class="form-control col-md-7 col-xs-12">
                            <ul class="parsley-errors-list filled" style="display:none" id="parsley-id-5"><li class="parsley-required">This value is required.</li></ul>
                        </div>
                    </div>
                  
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>


    
@endsection
@section('extrajs')
<script>
        $(document).ready(function(){
            $("#myInput").on("keyup", function() {
              var value = $(this).val().toLowerCase();
              $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
              });
            });
          });
</script>    
@endsection